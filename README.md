In this project I have tried to use most of the react.js concepts that I think are really cool.
The project stack is react.js for front end with basic routing done, redux for app state managment and firebase for auth and data storage. 
This is pretty much a serverless app you can say.

# Getting Started

Make sure you install all the packeages required by running below command:

### `npm install` or `npm i`

## Available Scripts

In the project directory, you can run:

### `yarn start` or `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test` or `npm test`

Launches the test runner in the interactive watch mode.\

### `yarn build` or `npm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!