import React, { Component } from 'react'
import {
    Button, Modal, Box, Typography, TextField,
} from '@mui/material'

import './bucket-modal.styles.css'

const taskModalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "20vw",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

class BucketModal extends Component {
    constructor(props) {
        super(props);
        this.props = props

        console.log("TaskModal props: ", this.props)
        this.state = {
            bucketInEdit: (props.bucketInEdit ? props.bucketInEdit : null),
            newBucketName: (props.bucketInEdit ? props.bucketInEdit : null)
        }
    }

    handleSubmitClick = () => {
        const { onBtnClick } = this.props
        const { bucketInEdit, newBucketName } = this.state

        onBtnClick({
            name: bucketInEdit,
            updatedName: newBucketName
        })
    }

    render() {
        const { openModal, handleCloseModal, editMode } = this.props
        const { newBucketName } = this.state

        return (
            <Modal
                open={openModal}
                onClose={handleCloseModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={taskModalStyle}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {editMode ? "Edit Bucket" : "Add Bucket"}
                    </Typography>

                    <TextField id="taskName" label="Name" variant="standard" className="inputField"
                        defaultValue={newBucketName}
                        onChange={(evt) => this.setState({
                            newBucketName: evt.target.value
                        })} />

                    <Button variant="outlined" onClick={this.handleSubmitClick}>{editMode ? "Save" : "Add"}</Button>
                </Box>
            </Modal>
        )
    }
}

export default BucketModal;