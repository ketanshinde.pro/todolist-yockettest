import React, { Component } from 'react'
import { Tooltip, IconButton } from '@material-ui/core'
import { DataGrid } from '@mui/x-data-grid'
import { Add, Delete, Edit } from '@material-ui/icons'
import { connect } from 'react-redux'
import BucketModal from '../bucket-modal/bucket-modal.component'
import './buckets.styles.css'

class Buckets extends Component {
    constructor(props) {
        super(props)

        console.log("props : ", props)
        this.props = props

        this.state = {
            openAddBucketModal: false,
            openEditBucketModal: false,
            bucketInEdit: null,
            columns: [
                { field: 'uid', headerName: 'ID', hide: true },
                { field: 'name', headerName: 'Name', width: 300 },
                {
                    field: 'actions', headerName: 'Actions', width: 200,
                    renderCell: (id) => (
                        <>
                            <Tooltip title="Edit" placement="top">
                                <IconButton onClick={() => this.clickedOnEdit(id)}>
                                    <Edit />
                                </IconButton>
                            </Tooltip>

                            <Tooltip title="Delete" placement="top">
                                <IconButton onClick={() => this.deleteBucket(id)}>
                                    <Delete />
                                </IconButton>
                            </Tooltip>
                        </>
                    )
                },
            ]
        }
    }

    handleOpenAddBucketModal = () => this.setState({ openAddBucketModal: true });
    handleCloseAddBucketModal = () => this.setState({ openAddBucketModal: false });
    addBucket = (bucketInfo) => {
        console.log("Editing the bucket ...", bucketInfo, this.props)
        const { dispatch } = this.props

        dispatch({
            type: "ADD_BUCKET",
            payload: bucketInfo
        })

        this.handleCloseAddBucketModal()
    }

    handleOpenEditBucketModal = () => this.setState({ openEditBucketModal: true });
    handleCloseEditBucketModal = () => this.setState({ openEditBucketModal: false });
    editBucket = (bucketInfo) => {
        console.log("Editing the bucket ...", bucketInfo, this.props)
        const { dispatch } = this.props

        dispatch({
            type: "EDIT_BUCKET",
            payload: bucketInfo
        })

        this.handleCloseEditBucketModal()
    }
    clickedOnEdit = (evt) => {
        console.log("clicked on edit bucket : ", evt)
        this.setState({ bucketInEdit: evt.id },
            () => this.handleOpenEditBucketModal())
    }

    deleteBucket = (rowInfo) => {
        console.log("clicked on delete : ", rowInfo)
        const { dispatch } = this.props

        dispatch({
            type: "DELETE_BUCKET",
            payload: rowInfo.id
        })
    }

    render() {
        const { bucketList } = this.props;
        const { columns, openAddBucketModal, openEditBucketModal, bucketInEdit } = this.state;

        let tabularBucketList = []
        for (let index = 0; index < bucketList.length; index++) {
            const element = bucketList[index];
            tabularBucketList.push({
                uid: element,
                name: element
            })
        }

        return (
            <div>
                <Tooltip title="Add Bucket" placement="top">
                    <IconButton className="addBucketBtn" size="large" onClick={this.handleOpenAddBucketModal}>
                        <Add fontSize="large" />
                    </IconButton>
                </Tooltip>
                <div className="bucketsRoot">
                    <DataGrid
                        rows={tabularBucketList}
                        columns={columns}
                        getRowId={(row) => row.uid}
                        pageSize={10}
                        rowsPerPageOptions={[10]}
                    />
                </div>

                {
                    openAddBucketModal ?
                        <BucketModal
                            openModal={openAddBucketModal}
                            handleCloseModal={this.handleCloseAddBucketModal}
                            onBtnClick={this.addBucket}
                            editMode={false}
                        />
                        :
                        null
                }

                {
                    openEditBucketModal ?
                        <BucketModal
                            openModal={openEditBucketModal}
                            handleCloseModal={this.handleCloseEditBucketModal}
                            onBtnClick={this.editBucket}
                            editMode={true}
                            bucketInEdit={bucketInEdit}
                        />
                        :
                        null
                }
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        bucketList: state.tasks.bucketList
    }
}

export default connect(mapStateToProps)(Buckets)