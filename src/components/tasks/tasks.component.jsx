import React, { Component } from 'react'
import { connect } from 'react-redux'
import { DataGrid } from '@mui/x-data-grid'
import './tasks.style.css'
import { IconButton, Tooltip } from '@mui/material'
import { Delete, Edit, Add } from '@material-ui/icons'
import TaskModal from '../task-modal/task-modal.component'

function mapStateToPros(state) {
    return {
        tasks: state.tasks.taskList
    }
}

class Tasks extends Component {
    constructor(props) {
        super(props)
        this.props = props

        this.state = {
            openAddTaskModal: false,
            openEditTaskModal: false,
            taskInEdit: null,
            openDeletetTaskModal: false,
            columns: [
                { field: 'uid', headerName: 'ID', hide: true },
                { field: 'name', headerName: 'Name', width: 300 },
                { field: 'bucket', headerName: 'Bucket', width: 200 },
                { field: 'priority', headerName: 'Priority', width: 150 },
                { field: 'deadline', headerName: 'Deadline', width: 200 },
                { field: 'reminder', headerName: 'Reminder', width: 200 },
                { field: 'status', headerName: 'Status', width: 200 },
                {
                    field: 'actions', headerName: 'Actions', width: 200,
                    renderCell: (id) => (
                        <>
                            <Tooltip title="Edit" placement="top">
                                <IconButton onClick={() => this.clickedOnEdit(id)}>
                                    <Edit />
                                </IconButton>
                            </Tooltip>

                            <Tooltip title="Delete" placement="top">
                                <IconButton onClick={() => this.deleteTask(id)}>
                                    <Delete />
                                </IconButton>
                            </Tooltip>
                        </>
                    )
                },
            ]
        }
    }

    handleOpenAddTaskModal = () => this.setState({ openAddTaskModal: true });
    handleCloseAddTaskModal = () => this.setState({ openAddTaskModal: false });
    addTask = (taskInfo) => {
        console.log("Adding the task ...", taskInfo, this.props)
        const { dispatch } = this.props

        dispatch({
            type: "ADD_TASK",
            payload: taskInfo
        })

        this.handleCloseAddTaskModal()
    }

    handleOpenEditTaskModal = () => this.setState({ openEditTaskModal: true });
    handleCloseEditTaskModal = () => this.setState({ openEditTaskModal: false });
    editTask = (taskInfo) => {
        console.log("Editing the task ...", taskInfo)
        const { dispatch } = this.props

        dispatch({
            type: "EDIT_TASK",
            payload: taskInfo
        })
        this.handleCloseEditTaskModal()
    }
    clickedOnEdit = (evt) => {
        console.log("clicked on edit task : ", evt)
        this.setState({ taskInEdit: evt.id },
            () => this.handleOpenEditTaskModal())
    }

    deleteTask = (evt) => {
        console.log("clicked on delete task : ", evt)
        const { dispatch } = this.props

        dispatch({
            type: "DELETE_TASK",
            payload: evt.id
        })
    }

    render() {
        const { tasks } = this.props;
        const { columns, openAddTaskModal, openEditTaskModal, taskInEdit } = this.state
        console.log("tasks : ", tasks)


        return (
            <div>
                <Tooltip title="Add Task" placement="top">
                    <IconButton className="addTaskBtn" size="large" onClick={this.handleOpenAddTaskModal}>
                        <Add fontSize="large" color="secondary"/>
                    </IconButton>
                </Tooltip>
                <div className="tasksRoot">
                    <DataGrid
                        rows={tasks}
                        columns={columns}
                        getRowId={(row) => row.uid}
                        pageSize={10}
                        rowsPerPageOptions={[10]}
                    />
                </div>

                {
                    openAddTaskModal ?
                        <TaskModal
                            openModal={openAddTaskModal}
                            handleCloseModal={this.handleCloseAddTaskModal}
                            onBtnClick={this.addTask}
                            editMode={false}
                        />
                        :
                        null
                }

                {
                    openEditTaskModal ?
                        <TaskModal
                            openModal={openEditTaskModal}
                            handleCloseModal={this.handleCloseEditTaskModal}
                            onBtnClick={this.editTask}
                            taskInEdit={taskInEdit}
                            editMode={true}
                        />
                        :
                        null
                }
            </div>
        )
    }
}

export default connect(mapStateToPros)(Tasks);