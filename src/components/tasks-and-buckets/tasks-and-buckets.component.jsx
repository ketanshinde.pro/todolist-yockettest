import React from 'react'
import { Tabs, Tab, Box } from '@mui/material';
import Buckets from '../buckets/buckets.component';
import Tasks from '../tasks/tasks.component';

export default function TasksAndBuckets() {
    const [value, setValue] = React.useState(0);
    const [componentToLoad, setComponentToLoad] = React.useState(0);

    const handleChange = (event, newValue) => {
        console.log("newValue : ", event.target.outerText)
        setValue(newValue);
        setComponentToLoad(event.target.outerText)
    };

    let ComponentToBeRendered = (<Tasks />)
    if (componentToLoad === "BUCKETS") {
        ComponentToBeRendered = (<Buckets />)
    }

    return (
        <div>
            <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
                <Tabs value={value} onChange={handleChange} centered>
                    <Tab label="Tasks" />
                    <Tab label="Buckets" />
                </Tabs>
            </Box>
            <div>
                {ComponentToBeRendered}
            </div>
        </div>
    )
}
