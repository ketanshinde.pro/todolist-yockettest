import React, { Component } from 'react'
import {
    Button, Modal, Box, Typography, TextField,
    Select, MenuItem, FormControl, InputLabel
} from '@mui/material'
import { uid } from 'uid';
import { connect } from 'react-redux';

import './task-modal.styles.css'

const taskModalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "20vw",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

class TaskModal extends Component {
    constructor(props) {
        super(props);
        this.props = props

        console.log("TaskModal props: ", this.props)
        this.state = {
            taskUid: (props.uid ? props.uid : uid(16)),
            taskName: (props.name ? props.name : null),
            taskDescription: (props.description ? props.description : null),
            taskPriority: (props.priority ? props.priority : "Low"),
            taskStatus: (props.status ? props.status : "Incomplete"),
            taskDeadLine: (props.deadline ? props.deadline : null),
            taskReminder: (props.reminder ? props.reminder : null),
            taskBucket: (props.bucket ? props.bucket : "Home"),
            priorities: ["Low", "Moderate", "High"],
            statuses: ["Incomplete", "Active", "Complete"],
            buketList: (props.buketList ? props.buketList : [])
        }
    }

    handlePriorityChange = (evt) => {
        this.setState({
            taskPriority: evt.target.value
        })
    }

    handleBucketChange = (evt) => {
        this.setState({
            taskBucket: evt.target.value
        })
    }

    handleStatusChange = (evt) => {
        this.setState({
            taskStatus: evt.target.value
        })
    }

    handleSubmitClick = () => {
        const { onBtnClick } = this.props
        const { taskUid, taskName, taskDescription, taskPriority, taskStatus, taskDeadLine,
            taskReminder, taskBucket
        } = this.state

        onBtnClick({
            uid: taskUid,
            name: taskName,
            description: taskDescription,
            priority: taskPriority,
            status: taskStatus,
            deadline: taskDeadLine,
            reminder: taskReminder,
            bucket: taskBucket,
        })
    }

    render() {
        const { openModal, handleCloseModal, editMode } = this.props
        const { taskName, taskDescription, taskPriority, taskDeadLine,
            taskReminder, taskBucket, taskStatus, priorities, buketList, statuses
        } = this.state

        return (
            <Modal
                open={openModal}
                onClose={handleCloseModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={taskModalStyle}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        {editMode ? "Edit Task" : "Add Task"}
                    </Typography>

                    <TextField id="taskName" label="Name" variant="standard" className="inputField"
                        defaultValue={taskName}
                        onChange={(evt) => this.setState({
                            taskName: evt.target.value
                        })} />

                    <TextField id="taskDescription" label="Description" multiline rows={4} className="inputField"
                        defaultValue={taskDescription}
                        onChange={(evt) => this.setState({
                            taskDescription: evt.target.value
                        })} />

                    <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }} className="inputField">
                        <InputLabel id="demo-simple-select-standard-label">Priority</InputLabel>
                        <Select
                            labelId="demo-simple-select-standard-label"
                            id="demo-simple-select-standard"
                            value={taskPriority}
                            onChange={this.handlePriorityChange}
                            label="Priority"
                        >
                            {priorities.map((item) => {
                                return (<MenuItem key={item} value={item}>{item}</MenuItem>)
                            })}
                        </Select>
                    </FormControl>

                    <InputLabel>Deadline</InputLabel>
                    <TextField id="taskDeadLine" type="datetime-local" variant="standard" className="inputField"
                        defaultValue={taskDeadLine}
                        onChange={(evt) => this.setState({
                            taskDeadLine: evt.target.value
                        })} />

                    <InputLabel>Reminder</InputLabel>
                    <TextField id="taskReminder" type="datetime-local" variant="standard" className="inputField"
                        defaultValue={taskReminder}
                        onChange={(evt) => this.setState({
                            taskReminder: evt.target.value
                        })} />

                    <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }} className="inputField">
                        <InputLabel id="demo-simple-select-standard-label">Task Bucket</InputLabel>
                        <Select
                            labelId="demo-simple-select-standard-label"
                            id="demo-simple-select-standard"
                            value={taskBucket}
                            onChange={this.handleBucketChange}
                            label="Priority"
                        >
                            {buketList.map((item) => {
                                return (<MenuItem key={item} value={item}>{item}</MenuItem>)
                            })}
                        </Select>
                    </FormControl>

                    <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }} className="inputField">
                        <InputLabel id="demo-simple-select-standard-label">Task Status</InputLabel>
                        <Select
                            labelId="demo-simple-select-standard-label"
                            id="demo-simple-select-standard"
                            value={taskStatus}
                            onChange={this.handleStatusChange}
                            label="Status"
                        >
                            {statuses.map((item) => {
                                return (<MenuItem key={item} value={item}>{item}</MenuItem>)
                            })}
                        </Select>
                    </FormControl>

                    <Button variant="outlined" onClick={this.handleSubmitClick}>{editMode ? "Save" : "Add"}</Button>
                </Box>
            </Modal>
        )
    }
}

function mapStateToProps(state, ownProps) {
    let task = state.tasks.taskList.find(task => task.uid === ownProps.taskInEdit)
    console.log("mapStateToProps task : ", task)
    return {
        buketList: state.tasks.bucketList,
        ...task
    }
}

export default connect(mapStateToProps)(TaskModal);