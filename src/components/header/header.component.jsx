import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button } from '@mui/material'
import { auth } from '../../firebase/firebase.utils'
import { withRouter } from 'react-router'
import './header.styles.css'

class Header extends Component {
    constructor(props) {
        super(props)

        this.props = props
    }

    render() {
        const { currentUser } = this.props

        return (
            <div className="headerRoot">
                <div className="divider">
                    <div className="dividerLine"></div>
                    <div className="dividerLineSeparator"></div>
                    <div className="dividerLine"></div>
                    <div className="dividerLineSeparator"></div>
                    <div className="dividerLine"></div>
                </div>
                <div className="headerTopicDiv">
                    <p className="headerText" >Yocket Test</p>
                </div>
                <div className="headerTopicDiv">
                    <p className="headerTopic">ToDo Application</p>
                </div>
                {
                    currentUser ?
                        <Button className="signOutBtn" onClick={() => {
                            auth.signOut()
                        }}><b>Sigh Out</b></Button>
                        :
                        null
                }
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.user.currentUser
    }
}

export default connect(mapStateToProps)(withRouter(Header))