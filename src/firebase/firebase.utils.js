import firebase from 'firebase/compat/app'
import 'firebase/compat/firestore'
import 'firebase/compat/auth'

const firebaseConfig = {
    apiKey: "AIzaSyBjI4Wkldo5hMMAZgAaM8BcJiURyjyVT7c",
    authDomain: "yocket-test-e351f.firebaseapp.com",
    projectId: "yocket-test-e351f",
    storageBucket: "yocket-test-e351f.appspot.com",
    messagingSenderId: "804525691732",
    appId: "1:804525691732:web:e8a9b1cdab9b83b0526ec1",
    measurementId: "G-PFSYCEY9HK"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return

    const userRef = firestore.doc(`users/${userAuth.uid}`)
    const snapshot = await userRef.get()
    console.log("snapshot : ", snapshot)

    if (!snapshot.exists) {
        const { displayName, email } = userAuth
        const createdAt = new Date()

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (error) {
            console.log("error while creating user : ", error)
        }
    }

    return userRef
}

firebase.initializeApp(firebaseConfig)

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({
    prompt: 'select_account'
})

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;