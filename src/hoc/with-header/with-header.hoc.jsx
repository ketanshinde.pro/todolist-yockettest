import React from 'react'
import { Divider } from '@material-ui/core'
import Header from '../../components/header/header.component'

const WithHeader = WrappedComponent => {
    const returnComponent = ({ topic }) => {
        return (
            <div>
                <Header topic={topic} />
                <Divider />
                <WrappedComponent />
            </div>
        )
    }
    return returnComponent
}

export default WithHeader