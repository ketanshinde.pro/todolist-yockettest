import React, { Component } from 'react'
import { TextField, Button } from '@material-ui/core'
import { Link, withRouter } from 'react-router-dom'
import { signInWithGoogle, auth } from '../../firebase/firebase.utils'
import './sign-in.styles.css'

class SignIn extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            emailError: false,
            emailErrorMessage: "Invalid Email ID",
            password: '',
            passwordError: false,
            passwordErrorMessage: "Password can not be empty !",
        }

        this.props = props
        console.log("this.props : ", this.props)
    }

    trySignInUser = async () => {
        const { email, password } = this.state
        const { history } = this.props
        try {
            await auth.signInWithEmailAndPassword(email, password)
            this.setState({
                email: '',
                password: ''
            })

            console.log("going home as logged in")
            history.push('/home')
        } catch (error) {
            console.log("trySignInUser error : ", error)
        }
    }

    onSignInClicked = () => {
        const { email, emailError, password, passwordError } = this.state
        var emailRegEx = /\S+@\S+\.\S+/;
        let isEmailValid = emailRegEx.test(email)
        if (isEmailValid && password !== "") {
            this.trySignInUser()

            this.setState({
                emailError: false,
                passwordError: false
            })
        }
        else {
            if (!isEmailValid) {
                this.setState({
                    emailError: true
                })
            }
            else {
                if (emailError) {
                    this.setState({
                        emailError: false
                    })
                }
            }

            if (password === "") {
                this.setState({
                    passwordError: true
                })
            }
            else {
                if (passwordError) {
                    this.setState({
                        passwordError: false
                    })
                }
            }
        }
    }

    render() {
        const { email, emailError, emailErrorMessage,
            password, passwordError, passwordErrorMessage } = this.state

        return (
            <div className='signInRoot'>
                <TextField
                    defaultValue={email}
                    id="standard-basic"
                    label="Email"
                    variant="standard"
                    error={emailError}
                    helperText={emailError ? emailErrorMessage : ''}
                    onChange={(evt) => {
                        this.setState({ email: evt.target.value })
                    }}
                    className="formInputs"
                />

                <TextField
                    defaultValue={password}
                    id="standard-basic"
                    label="Password"
                    variant="standard"
                    error={passwordError}
                    helperText={passwordError ? passwordErrorMessage : ''}
                    onChange={(evt) => {
                        this.setState({ password: evt.target.value })
                    }}
                    className="formInputs"
                />

                <Button className="formInputs" onClick={this.onSignInClicked}>Sign In</Button>
                <Button onClick={signInWithGoogle} className="formInputs">Sign In With Google</Button>
                <Link to='./sign-up'>Sign Up</Link>
            </div>
        )
    }
}

export default withRouter(SignIn);