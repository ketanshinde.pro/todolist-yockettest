import React, { Component } from 'react'
import { TextField, Button } from '@material-ui/core'
import { Link, withRouter } from 'react-router-dom'
import { auth, createUserProfileDocument } from '../../firebase/firebase.utils'
import './sign-up.styles.css'

class SignUp extends Component {
    constructor(props) {
        super(props)

        this.state = {
            displayName: '',
            displayNameError: false,
            displayNameErrorMessage: 'Name can not be empty !',
            email: '',
            emailError: false,
            emailErrorMessage: 'Invalid Email ID',
            password: '',
            passwordError: false,
            passwordErrorMessage: 'Password can not be empty !',
            confirmPassword: '',
            confirmPasswordError: false,
            confirmPasswordErrorMessage: 'Confirm password does not match with Password !',
        }
    }

    tryCreatingUser = async () => {
        const { displayName, email, password } = this.state
        const { history } = this.props

        try {
            const { user } = await auth.createUserWithEmailAndPassword(email, password)
            await createUserProfileDocument(user, { displayName })

            this.setState({
                displayName: '',
                email: '',
                password: '',
                confirmPassword: '',
            })

            history.push('/home')
        } catch (error) {
            console.log("tryCreatingUser error : ", error)
        }
    }

    onSignUpClicked = () => {
        const { displayName, email, emailError, password, displayNameError,
            passwordError, confirmPassword, confirmPasswordError } = this.state

        var emailRegEx = /\S+@\S+\.\S+/;
        let isEmailValid = emailRegEx.test(email)
        let passwordIsNotEmpty = password !== ""
        let confirmPasswordIsMatch = confirmPassword === password
        let displayNameIsNotEmpty = displayName !== ""

        if (displayNameIsNotEmpty && isEmailValid && passwordIsNotEmpty && confirmPasswordIsMatch) {
            this.setState({
                emailError: false,
                passwordError: false,
                confirmPasswordError: false,
                displayNameError: false
            })

            this.tryCreatingUser()
        }
        else {
            if (!isEmailValid) {
                this.setState({
                    emailError: true
                })
            }
            else {
                if (emailError) {
                    this.setState({
                        emailError: false
                    })
                }
            }

            if (!passwordIsNotEmpty) {
                this.setState({
                    passwordError: true
                })
            }
            else {
                if (passwordError) {
                    this.setState({
                        passwordError: false
                    })
                }
            }

            if (!confirmPasswordIsMatch) {
                this.setState({
                    confirmPasswordError: true
                })
            }
            else {
                if (confirmPasswordError) {
                    this.setState({
                        confirmPasswordError: false
                    })
                }
            }

            if (!displayNameIsNotEmpty) {
                this.setState({
                    displayNameError: true
                })
            }
            else {
                if (displayNameError) {
                    this.setState({
                        displayNameError: false
                    })
                }
            }
        }
    }

    render() {
        const { displayName, displayNameError, displayNameErrorMessage, email, emailError, emailErrorMessage,
            password, passwordError, passwordErrorMessage,
            confirmPassword, confirmPasswordError, confirmPasswordErrorMessage } = this.state

        return (
            <div className='signUpRoot'>

                <TextField
                    defaultValue={displayName}
                    id="standard-basic"
                    label="Name"
                    variant="standard"
                    error={displayNameError}
                    helperText={displayNameError ? displayNameErrorMessage : ''}
                    onChange={(evt) => {
                        this.setState({ displayName: evt.target.value })
                    }}
                    className="formInputs"
                    required
                />

                <TextField
                    defaultValue={email}
                    id="standard-basic"
                    label="Email"
                    variant="standard"
                    error={emailError}
                    helperText={emailError ? emailErrorMessage : ''}
                    onChange={(evt) => {
                        this.setState({ email: evt.target.value })
                    }}
                    className="formInputs"
                    required
                />

                <TextField
                    defaultValue={password}
                    id="standard-basic"
                    type="password"
                    label="Password"
                    variant="standard"
                    error={passwordError}
                    helperText={passwordError ? passwordErrorMessage : ''}
                    onChange={(evt) => {
                        this.setState({ password: evt.target.value })
                    }}
                    className="formInputs"
                    required
                />

                <TextField
                    defaultValue={confirmPassword}
                    id="standard-basic"
                    type="password"
                    label="Confirm Password"
                    variant="standard"
                    error={confirmPasswordError}
                    helperText={confirmPasswordError ? confirmPasswordErrorMessage : ''}
                    onChange={(evt) => {
                        this.setState({ confirmPassword: evt.target.value })
                    }}
                    className="formInputs"
                    required
                />

                <Button className="formInputs" onClick={this.onSignUpClicked}>Sign Up</Button>
                <Link to='./sign-in'>Sign In</Link>
            </div>
        )
    }
}

export default withRouter(SignUp);