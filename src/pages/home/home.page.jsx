import React, { Component } from 'react'
import TasksAndBuckets from '../../components/tasks-and-buckets/tasks-and-buckets.component'

export default class Home extends Component {
    render() {
        return (
            <TasksAndBuckets />
        )
    }
}
