const initialState = {
    taskList: [
        {
            uid: "1",
            name: "Go to bank",
            description: "Go to bank to get bank statement.",
            priority: "Low",
            status: "Incomplete",
            deadline: "2021-10-10T16:12",
            reminder: "2021-10-09T16:11",
            bucket: "Work",
        },
        {
            uid: "2",
            name: "Visit kids school",
            description: "Go to kids school to talk to principal.",
            priority: "Moderate",
            status: "Incomplete",
            deadline: "2021-10-12T16:12",
            reminder: "2021-10-11T16:11",
            bucket: "Home"
        }
    ],
    bucketList: ["Home", "Work", "Public"]
}

export default function tasks(state = initialState, action) {
    let taskList = (state ? [...state.taskList] : [])
    let bucketList = (state ? [...state.bucketList] : [])
    let foundIndex;
    // console.log("action : ", action)
    switch (action.type) {
        case "ADD_TASK":
            taskList.push(action.payload)
            return { ...state, taskList: taskList }
        case "EDIT_TASK":
            foundIndex = taskList.findIndex(task => task.uid === action.payload.uid);
            taskList[foundIndex] = action.payload
            return { ...state, taskList: taskList }
        case "DELETE_TASK":
            foundIndex = taskList.findIndex(task => task.uid === action.payload);
            console.log("foundIndex : ", foundIndex)
            taskList.splice(foundIndex, 1);
            return { ...state, taskList: taskList }
        case "ADD_BUCKET":
            bucketList.push(action.payload.updatedName)
            return { ...state, bucketList: bucketList }
        case "EDIT_BUCKET":
            foundIndex = bucketList.findIndex(bucket => bucket === action.payload.name);
            bucketList[foundIndex] = action.payload.updatedName
            return { ...state, bucketList: bucketList }
        case "DELETE_BUCKET":
            foundIndex = bucketList.findIndex(bucket => bucket === action.payload);
            bucketList.splice(foundIndex, 1);
            return { ...state, bucketList: bucketList }
        default:
            return state;
    }
}