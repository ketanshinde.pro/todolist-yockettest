const initialState = {
    currentUser: null,
}

export default function user(state = initialState, action) {
    // console.log("action : ", action)
    switch (action.type) {
        case "SET_CURRENT_USER":
            return { ...state, currentUser: action.payload }
        default:
            return state;
    }
}