import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './reducer'

export default function configureStore(preloadedState) {
    const composedEnhancers = composeWithDevTools()
    const store = createStore(rootReducer, preloadedState, composedEnhancers)
    return store
}

// export default function configureStore(preloadedState) {
//     const store = createStore(rootReducer, preloadedState)
//     return store
// }