export const ADD_TASK = "ADD_TASK";
export const EDIT_TASK = "EDIT_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const ADD_BUCKET = "ADD_BUCKET";
export const EDIT_BUCKET = "EDIT_BUCKET";
export const DELETE_BUCKET = "DELETE_BUCKET";