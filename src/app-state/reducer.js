import { combineReducers } from "redux";
import tasks from './reducers/tasks'
import user from './reducers/user'

let rootReducer = combineReducers({
    tasks: tasks,
    user: user
});

export default rootReducer