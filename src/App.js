import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { connect } from 'react-redux'
import { SET_CURRENT_USER } from './app-state/actions/user';
import './App.css';

import Home from './pages/home/home.page'
import SignIn from './pages/sign-in/sign-in.page';
import SignUp from './pages/sign-up/sign-up.page';
import WithHeader from './hoc/with-header/with-header.hoc';

import { auth, createUserProfileDocument } from './firebase/firebase.utils';

const HomeWithHeader = WithHeader(Home)
const SignInWithHeader = WithHeader(SignIn)
const SignUpWithHeader = WithHeader(SignUp)

class App extends Component {
  constructor(props) {
    super(props)

    this.props = props
  }

  unsubscribeFromAuth = null

  componentWillUnmount() {
    this.unsubscribeFromAuth()
  }

  componentDidMount() {
    const { dispatch } = this.props

    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {

      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth);
        // console.log("userRef : ", userRef);

        userRef.onSnapshot(snapShot => {
          // console.log("userRef snapShot data : ", snapShot.data())

          dispatch({
            type: SET_CURRENT_USER,
            payload: {
              id: snapShot.id,
              ...snapShot.data()
            }
          })
        })
      }
      else {
        dispatch({
          type: SET_CURRENT_USER,
          payload: userAuth
        })
      }
      // console.log("userAuth : ", userAuth)
    })
  }

  render() {
    const { currentUser } = this.props

    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={() => { return <Redirect to='home' /> }}></Route>
          <Route path='/home' render={() => {
            if (currentUser) {
              return <HomeWithHeader />
            }
            else {
              return <Redirect to='sign-in' />
            }
          }} />
          <Route path='/sign-in' render={() => {
            if (currentUser) {
              return <Redirect to='home' />
            }
            else {
              return <SignInWithHeader />
            }
          }} />
          <Route path='/sign-up' render={() => {
            if (currentUser) {
              return <Redirect to='home' />
            }
            else {
              return <SignUpWithHeader />
            }
          }} />
        </Switch>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.user.currentUser
  }
}

export default connect(mapStateToProps)(App);
